== Module und Blueprints

Samu ist besorgt, dass JHipster es ihm schwer macht, Konfigurationen zu ändern und
Erweiterungen nicht einfach möglich sind.
Jennifer kann ihn beruhigen.
Da JHipster im Kern eine Spring Boot Anwendung ist, können alle Konfigurationen durch modifizierte 
Versionen überschrieben oder erweitert werden.
Außerdem versucht JHipster die einzelnen Technologien in der Standardkonfiguration zu verwenden
und Modifikationen nur vorzunehmen, wenn diese unbedingt notwendig sind <<jhpolicies>>.
Dadurch sind Anpassungen einfach möglich.
Im Zweifel genügt ein Blick in die Dokumentation des entsprechenden (Upstream) Projektes.

Falls Samu größere Anpassungen vornehmen möchte, kann er auch ein Modul <<jhmodules>> oder einen Blueprint <<jhblueprints>>
entwickeln und JHipser Online on premise installieren, sodass automatisch immer 
sein spezielles Modul zusätzlich bei der Generierung einer Anwendung verwendet wird.

Ein Modul hat Zugriff auf die Konfiguration der JHipster Anwendung und kann, wie zum Beispiel ein Subgenerator,
Dateien anlegen. 
Über bestimmte Erweiterungspunkte (sog. `needles`) kann ein Modul bestehende Dateien erweitern oder modifizieren.
Es können beispielsweise neue Menüpunkte oder weitere Maven/Gradle Dependencies eingefügt werden.

Im Unterschied zu einem Modul kann ein Blueprint existierende Dateien überschreiben oder löschen und eine eigene Menge
von Dateien ausliefern.
Damit ist es dem Autor eines Blueprints möglich, u.a. Spring Boot durch ein anderes Framework zu ersetzen,
die Spring Security Konfiguration an die Bedürfnisse des eigenen Unternehmens anzupassen oder 
Java durch Kotlin zu ersetzen <<jhkotlin>>.
Module sind seit JHipster 3 verfügbar, Blueprints erst seit Version 5 (als Beta).
Daher ist die Auswahl an Blueprints noch nicht sehr groß.

Seit Version 6 ist der offizielle Blueprint um Vue.js als Client-Side Framework verwenden zu können in Version 1.0.0 verfügbar <<jhvuejs>>.
