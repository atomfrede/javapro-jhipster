= Getting Hip with JHipster
Frederik Hahne <frederik.hahne@wescale.com> <twitter: atomfrede>
{version}, {localdate}
:icons: font
:lang: de
ifndef::imagesdir[:imagesdir: images]
:favicon: ./{imagesdir}/favicon.ico
include::locale/attributes.adoc[]

JHipster ist eine offenen Entwicklugsplatform zur Entwicklung von modernen, Spring Boot basierten Webanwendungen mit Angular, React oder Vue 
Frontend zu entwicklen. 
Dieser Artikel beschreibt, wie JHipster helfen kann sich bei der Entwicklung auf die Kernfunktionen zu fokussieren ohne sich um das 
zeitraubende Setup von z.B. Datenbanken, Buildsystem oder Deployment in der Cloud zu kümmern.

Der Scrum Master Samu hat ein Problem. Er kann nicht noch mehr Funktionen aus dem unternehmensweiten Ticketsystem herausholen.
Daher spielt er mit dem Gedanken eine Webanwendung zu schreiben, die sich mit dem bestehenden Ticketsystem integriert und
er dort spezielle Anforderungen und Workflows implementieren kann.
Leider liegen seine Tage als Entwickler etwas weiter zurück und er hat auch keine Zeit sich mit dem Schreiben von
Boilerplate Code einer modernen Webanwendung zu befassen. 
Zum Glück erinnert er sich, dass die Java-Entwicklerin Jennifer in ihrer Freizeit im JHipster Projekt aktiv ist.
In diesem Artikel begleiten wir Samu dabei, wie er die Möglichkeiten von JHipster erkundet, um einen ersten Prototypen zu bauen und diesen 
am Ende in der Cloud zu betreiben.

include::_was-ist-jhipster.adoc[]

include::_installation.adoc[]

include::_entities.adoc[]

include::_ci-cd-deploy.adoc[]

include::_outlook.adoc[]

include::_author-info.adoc[]

[bibliography]
== Quellen

- [[[jhweb, 1]]] https://jhipster.tech
- [[[jhgh, 2]]] https://github.com/jhipster/generator-jhipster
- [[[fullstack, 3]]] https://www.packtpub.com/application-development/full-stack-development-jhipster
- [[[jhstats, 4]]] https://www.openhub.net/p/generator-jhipster
- [[[jhusing, 5]]] https://www.jhipster.tech/companies-using-jhipster/
- [[[jhinstall, 6]]] https://www.jhipster.tech/installation/
- [[[jhonline, 7]]] https://start.jhipster.tech
- [[[jhpolicies, 8]]] https://www.jhipster.tech/policies/
- [[[jhmodules, 9]]] https://www.jhipster.tech/modules/creating-a-module/
- [[[jhblueprints, 10]]] https://www.jhipster.tech/modules/creating-a-blueprint/
- [[[jhkotlin, 11]]] https://github.com/jhipster/jhipster-kotlin
- [[[jhvuejs, 12]]] https://github.com/jhipster/jhipster-vuejs
- [[[jhreleasenotes, 13]]] https://www.jhipster.tech/2019/05/02/jhipster-release-6.0.0.html
- [[[jhdevbox, 14]]] https://github.com/jhipster/jhipster-devbox
- [[[sonarsample, 15]]] https://sonarcloud.io/dashboard?id=io.github.jhipster.sample%3Ajhipster-sample-application
- [[[jhtesting, 16]]] https://www.jhipster.tech/running-tests/
- [[[jhentity, 17]]] https://www.jhipster.tech/creating-an-entity/
- [[[jdlstudio, 18]]] https://start.jhipster.tech/jdl-studio/
- [[[jhproduction, 19]]] https://www.jhipster.tech/production/
- [[[jhheroku, 20]]] https://www.jhipster.tech/heroku/
- [[[jhcicid, 21]]] https://www.jhipster.tech/setting-up-ci/
- [[[samplecode, 22]]] https://gitlab.com/atomfrede/javapro-jhipster-sample
- [[[sonarcloud, 23]]] https://sonarcloud.io/dashboard?id=javaprosample
- [[[deployedapplication, 24]]] https://javaprosample.herokuapp.com/
